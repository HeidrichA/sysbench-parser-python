#!/bin/bash

docker-compose pull
docker save $(docker-compose config|awk '/image: / {print $2} ') > Images.tgz
echo Create $(basename $(dirname $(realpath $0)))
tar czvf ../$(basename $(dirname $(realpath $0))).tgz --exclude .history .

