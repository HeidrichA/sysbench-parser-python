cd $(dirname $0)

set -e

docker-compose down -v
docker-compose run sys
docker-compose run db
docker-compose down -v
