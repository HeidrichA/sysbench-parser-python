import re
import yaml
import sys
import texttable
import textgraph


def sbout2list(stream):
    result = {}
    record = {}
    test_name = ""
    for line in stream:
        line = line.strip()
        # Look for start of the test, line inserted by a wrapper sript, nor sysbench itself
        # since sysbench doesn't echo the test name and parameters
        # without wrapper I have no idea how to identify the tests
        test_start = re.match(
            "^Run (?P<test_group>.+) test(?P<test_type>.+)?$", line)
        if test_start:
            if record:
                result.update({test_name: record})
                record = {}
            test_name = test_start.groupdict()["test_group"]
            if test_start.groupdict()["test_type"]:
                test_name = test_name + test_start.groupdict()["test_type"]
            continue
        # look for normal test results
        test_result = (re.match(
            "^(?P<metric>General statistics|Latency\(ms\)|File operations|Throughput|Threads fairness):", line))
        if test_result:
            metric = test_result.groupdict()["metric"]
            record[metric] = {}
            while True:
                line = stream.readline().strip()
                if not line:
                    break
                # Check single-, or dual-metric result line
                # total number of events:              94464155 --> {"total number of events": 94464155}
                # execution time (avg/stddev):   6.5683/0.03 --> {"execution time": {"avg": 6.5683}, {"stddev": 0.03}}
                # all numbers are float
                a = re.match("^\s*(?P<key>.+):\s*(?P<value>[\d.]+).?$", line)
                if a:
                    d = a.groupdict()
                    record[metric].update({d["key"]: float(d["value"])})
                    continue
                a = re.match(
                    "^\s*(?P<submetric>.+) \((?P<k1>\w+)/(?P<k2>\w+)\):\s*(?P<v1>[\d.]+)/(?P<v2>[\d.]+)\s*$", line)
                if a:
                    d = a.groupdict()
                    record[metric].update(
                        {d["submetric"]: {d["k1"]: float(d["v1"]), d["k2"]: float(d["v2"])}})
            continue  # next iteration in for:
        # Memory results are displayed very differently:
        # Total operations: 94464155 (9444964.25 per second)
        # then (after one empty line)
        # 92250.15 MiB transferred (9223.60 MiB/sec)
        mem_result_total = re.match(
            "^Total operations: (?P<ops>\d+) \((?P<opsPerS>[\d.]+) per second\)", line)
        if mem_result_total:
            d = mem_result_total.groupdict()
            record["transfer"] = {key: float(d[key]) for key in d}
            line = stream.readline().strip()  # should be empty
            assert line == ""
            line = stream.readline().strip()
            mem_result_transfer = re.match(
                "^(?P<MB>[\d.]+) MiB transferred \((?P<MBPS>[\d.]+) MiB/sec\)$", line)
            d = mem_result_transfer.groupdict()
            record["transfer"].update({key: float(d[key]) for key in d})
            continue
    result.update({test_name: record})
    return(result)


if __name__ == "__main__":
    result: dict = sbout2list(sys.stdin)
    # print(yaml.dump(result, indent=2))
    # for test in result:
    #     t = texttable.Texttable()
    #     print(f"{test}\n{'-'*len(test)}")
    #     t.header(["test_name", "metric", "value"])
    #     for sub in result[test]:
    #         for metric in result[test][sub]:
    #             t.add_row([sub, metric, result[test][sub][metric]])
    #     print(t.draw())

    print("I/O performance (mounted directory)")
    io_perf = texttable.Texttable(150)
    io_perf.header([""] + [re.match("^fileio \((?P<test>.+)\) in (?P<dir>.+).*$", t).groupdict()["test"]
                           for t in result if "mnt-test" in t])
    for metric in ['reads/s', 'read, MiB/s', 'writes/s', 'written, MiB/s']:
        io_perf.add_row([metric] + [result[t][s][m]
                                    for t in result if "mnt-test" in t
                                    for s in result[t]
                                    for m in result[t][s] if m == metric])
    print(io_perf.draw())

    print("I/O performance (docker volume)")
    io_perf = texttable.Texttable(150)
    io_perf.header([""] + [re.match("^fileio \((?P<test>.+)\) in (?P<dir>.+).*$", t).groupdict()["test"]
                           for t in result if "vol-test" in t])
    for metric in ['reads/s', 'read, MiB/s', 'writes/s', 'written, MiB/s']:
        io_perf.add_row([metric] + [result[t][s][m]
                                    for t in result if "vol-test" in t
                                    for s in result[t]
                                    for m in result[t][s] if m == metric])
    print(io_perf.draw())

    print("Computing performance")
    compute_perf = texttable.Texttable(max_width=150)
    compute_perf.header(
        [""] + [t for t in result if not t.startswith('fileio')])
    compute_perf.add_row(["Event per sec"] + [result[t]["General statistics"]["total number of events"] /
                         result[t]["General statistics"]["total time"] for t in result if not t.startswith('fileio')])
    print(compute_perf.draw())
    print("Computing performance")
    compute_perf = texttable.Texttable(max_width=150)
    compute_perf.header(
        [""] + [t for t in result if not t.startswith('fileio')])
    compute_perf.add_row(["Ops/s"] + [result[t]["General statistics"]["total number of events"] /
                         result[t]["General statistics"]["total time"] for t in result if not t.startswith('fileio')])
    compute_perf.add_row(["Memory BW (MiB/s)"] + [result[t]["transfer"]["MBPS"]
                         if "transfer" in result[t] else "" for t in result if not t.startswith('fileio')])
    print(compute_perf.draw())
