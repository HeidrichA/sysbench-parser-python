echo 'Running pgbench...'
echo 'initialize'
pgbench -U postgres -i postgres
echo 'Run for a while with regular reports'
pgbench -U postgres -P 10 -T 120 postgres

echo -n 'Tests are over, exit now with an '
/bin/false
