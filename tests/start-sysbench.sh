#!/bin/bash

function fileio() {
    cd ${1-tmp}
    for mode in seqwr seqrd rndrd rndwr rndrw; do
        sysbench "$COMMON" fileio prepare >/dev/null
        printf "\n----------\nRun fileio test (${mode}) in $1\n----------\n"
        sysbench "$COMMON" fileio --file-fsync-all=on --file-test-mode=${mode} run
        printf "\n----------\nEnd of fileio test (${mode}) in $1\n----------\n"
    done
    sysbench fileio cleanup
}

COMMON="--time=${TEST_TIME}"

for test in cpu memory mutex threads; do
    printf "\n----------\nRun ${test} test\n----------\n"
    sysbench "$COMMON" ${test} --threads=${CPUS} run
    printf "\n----------\nEnd of ${test} test\n----------\n"
done

cd /mnt-test
for dir_name in "/mnt-test" "/vol-test"; do
    fileio ${dir_name}
done
